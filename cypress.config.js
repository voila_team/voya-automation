const { defineConfig } = require('cypress')

module.exports = defineConfig({
  defaultCommandTimeout: 15000,
  requestTimeout: 25000,
  responseTimeout: 240000,
  video: true,
  viewportWidth: 1920,
  viewportHeight: 1080,
  chromeWebSecurity: false,
  numTestsKeptInMemory: 10,
  retries: {
    runMode: 2,
    openMode: 0,
  },
  env: {
    webappUrl: "http://localhost:4200",
    agencyUrl: "http://localhost:4201",
    adminEmail: "admin.qa@vtest.com",
    agentUserForCarBooking: "e2e.testing+agent001@getvoila.io",
    agentUserForFlightBooking: "e2e.testing+agent002@getvoila.io",
    agentUserForTrainBooking: "e2e.testing+agent003@getvoila.io",
    agentUserForHotelBooking: "e2e.testing+agent004@getvoila.io",
    agentUserForHotelBookingMultipleOptions: "e2e.testing+agent005@getvoila.io",
    agentUserForFlightAutomation: "e2e.testing+agent006@getvoila.io",
    agentUserForHotelAutomation: "e2e.testing+agent007@getvoila.io",
    agentUserForTrainBookingMultipleOptions: "e2e.testing+agent008@getvoila.io",
    agentUserForFlightBookingMultipleOptions: "e2e.testing+agent009@getvoila.io",
    userForCarCrossOrigin: "e2e.testing+user046@getvoila.io",
    userForFlightCrossOrigin: "e2e.testing+user047@getvoila.io",
    userForTrainCrossOrigin: "e2e.testing+user048@getvoila.io",
    userForHotelCrossOrigin: "e2e.testing+user049@getvoila.io",
    userForHotelCrossOriginMultipleOptions: "e2e.testing+user050@getvoila.io",
    userForFlightAutomation: "e2e.testing+user059@getvoila.io",
    userForHotelAutomation: "e2e.testing+user061@getvoila.io",
    userForTrainCrossOriginMultipleOptions: "e2e.testing+user062@getvoila.io",
    userForFlightCrossOriginMultipleOptions: "e2e.testing+user063@getvoila.io",
    usernameForPersonalSettingChanges: "cyvoya.ncfng@gmail.com",
    additionalTraveler: "QA User 006",
    password: "testing2022",
    adminPassword: "Parola123%",
  },
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
  },
})
