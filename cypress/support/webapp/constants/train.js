export const outbound = 'Hamburg Hbf';
export const outward = {
    ulm: 'Ulm Hbf',
    hamburg: 'Hamburg Hbf',
    berlin: 'Berlin',
}
export const inbound = {
    berlin: 'Berlin Hbf',
    ulm: 'Ulm',
}
export const firstClass = '1st class';
export const secondClass = '2nd class';
export const oneWay = 'onewayTrain';
export const roundtrip = 'roundtripTrain';
export const serviceType = 'train';
export const seatReservation = {
    yes: 'Yes',
    no: 'No',
}
export const ticketType = {
    first: 'First',
    superSparpries: 'Super Sparpreis',
    sparpreis: ' Sparpreis',
}
