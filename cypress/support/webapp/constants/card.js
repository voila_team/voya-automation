export const cardNumber = '4242424242424242';
export const cardHolderName = {
    generalName: 'Voya Card Name',
    flightName: 'Flight Card Name',
    trainName: 'Train Card Name',
    hotelName: 'Hotel Card Name',
    rentalCarName: 'Rental Car Card Name',
};
