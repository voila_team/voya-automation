export const location = 'Bucharest';
export const areaType = 'Area';
export const hotelType = 'Hotel';
export const hotelName = {
    mercureBucharest: 'Mercure Bucharest Unirii',
    internationalBucharest: 'International Bucharest City Centre Hotel, Bucharest',
};
export const serviceType = 'hotel';
