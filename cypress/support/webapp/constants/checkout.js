export const approvalReason = 'Test approval reason';
export const projectNumber = '001';
export const serviceType = {
    flight: 'flight',
    hotel: 'hotel',
    trains: 'trains',
}
