export const departure = {
    newYork: 'New York',
    hamburg: 'Hamburg',
    london: 'London',
};
export const destination = {
    losAngeles: 'Los Angeles',
    berlin: 'Berlin',
    paris: 'Paris',
};
export const oneway = 'oneway';
export const roundtrip = 'roundtrip';
export const serviceType = 'flight';
