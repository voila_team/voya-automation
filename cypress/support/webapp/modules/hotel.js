import { checkout } from './checkout';
import { datePicker } from './datePicker';
import { common } from "./common";
import { serviceType } from "../constants/hotel";
import { cardHolderName } from "../constants/card";

export class Hotel {

    bookHotel(location, locationType, approvalReason, projectNumber, additionalTraveler) {
        cy.intercept('GET', '**/backend/api/users/company-traveller**').as('companyTraveller');
        common.clickOnChatBtn();
        cy.url().should('include', '/chat');
        common.clickOnHotelBtnFromChat();
        cy.url().should('include', '/chat?search=hotel');
        cy.wait('@companyTraveller').then((company) => {
            expect(company.response.statusCode).to.equal(200);
        });
        this.selectLocation(location, locationType);
        cy.wait('@companyTraveller').then((company) => {
            expect(company.response.statusCode).to.equal(200);
        });
        datePicker.setCheckinAndCheckoutDate();
        if (typeof additionalTraveler !== "undefined") {
            common.addTraveler(additionalTraveler);
        }
        this.clickOnSearchHotels();
        cy.intercept('**/api/hotel/all-options/**').as('searchHotels');
        cy.intercept('**/api/cards*').as('cards');
        cy.intercept('**/approval/summary*').as('summary');
        cy.intercept('**/error-summary*').as('errorSummary');
        cy.wait('@searchHotels').then((hotels) => {
            expect(hotels.response.statusCode).to.equal(200);
        });
        this.selectHotel(location);
        cy.wait('@cards').then((cards) => {
            expect(cards.response.statusCode).to.equal(200);
        });
        cy.wait('@summary').then((summary) => {
            expect(summary.response.statusCode).to.equal(200);
        });
        cy.wait('@errorSummary').then((errorSummary) => {
            expect(errorSummary.response.statusCode).to.equal(200);
        });
        checkout.checkOutConfirmation(approvalReason, projectNumber, serviceType, cardHolderName.hotelName);
    }

    selectLocation(location, locationType) {
        cy.get('app-hotel-location-autocomplete')
            .find('[data-cy="instantOptionsHotelsLocationSelect"]')
            .should('be.visible')
            .click()
            .type(location);

        cy.get('app-hotel-location-autocomplete ng-dropdown-panel .ng-option .option-inner ')
            .find('.option-tag')
            .contains(locationType)
            .parent()
            .find('.option-text')
            .contains(location)
            .should('exist')
            .eq(0)
            .click();
    }

    clickOnSearchHotels() {
        cy.get('[data-cy="confirmAndSendRequestButton"]')
            .should('be.visible')
            .click();
    }

    selectHotel(hotelName) {
        cy.intercept('**/api/hotel/all-offers/**').as('offers');
        if (hotelName === 'Mercure Bucharest Unirii') {
            cy.contains('app-hotel-search-result', 'Mercure Bucharest Unirii').within(() => {
                cy.get('[data-cy=seeAvailableRoomsButton]')
                    .invoke('attr', 'class').then($el => {
                        const isDisabled = $el.toString();
                        cy.wrap(isDisabled).as('button');
                    })
            })
            cy.get('@button').then(($el) => {
                const availableRooms = $el.toString().includes('disabled');
                if (availableRooms) {
                    cy.contains('app-hotel-search-result', 'International Bucharest City Centre Hotel').within(() => {
                        cy.get('[data-cy=seeAvailableRoomsButton]:not(:disabled)')
                            .scrollIntoView()
                            .should('be.visible')
                            .invoke('removeAttr', 'target')
                            .click();
                    })
                } else {
                    cy.contains('app-hotel-search-result', 'Mercure Bucharest Unirii').within(() => {
                        cy.get('[data-cy=seeAvailableRoomsButton]:not(:disabled)')
                            .scrollIntoView()
                            .should('be.visible')
                            .invoke('removeAttr', 'target')
                            .click();
                    })
                }
            })
            cy.wait('@offers').then((offers) => {
                expect(offers.response.statusCode).to.equal(200);
            });
            cy.get('[data-cy=selectRoomButton]:not(:disabled)')
                .last()
                .should('exist')
                .click();
        } else {
            cy.get('[data-cy=seeAvailableRoomsButton]:not([disabled])')
                .last()
                .scrollIntoView()
                .should('be.visible')
                .invoke('removeAttr', 'target')
                .click();
            cy.wait('@offers').then((offers) => {
                expect(offers.response.statusCode).to.equal(200);
            });
            cy.get('[data-cy=selectRoomButton]:not(:disabled)')
                .last()
                .should('exist')
                .click();
        }
    }



    confirmOptionSendViaChat(approvalReason, projectNumber) {
        common.clickOnChatBtn();
        cy.intercept('**/api/cards*').as('cards');
        cy.intercept('**/approval/summary*').as('summary');
        cy.intercept('**/error-summary*').as('errorSummary');
        cy.get('app-chat-hotel-option')
            .last()
            .scrollIntoView()
            .should('be.visible')
            .click();
        cy.get('app-extended-option')
            .should('be.visible').within(() => {
            cy.contains('button', 'Select')
                .should('be.visible')
                .click();
        });
        cy.wait('@cards').then((cards) => {
            expect(cards.response.statusCode).to.equal(200);
        });
        cy.wait('@summary').then((summary) => {
            expect(summary.response.statusCode).to.equal(200);
        });
        cy.wait('@errorSummary').then((errorSummary) => {
            expect(errorSummary.response.statusCode).to.equal(200);
        });
        checkout.checkOutConfirmation(approvalReason, projectNumber, serviceType, cardHolderName.hotelName);
    }
}

export const hotel = new Hotel();
