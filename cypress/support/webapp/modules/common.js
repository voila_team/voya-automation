export class Common {

    clickOnChatBtn() {
        cy.get('[data-cy="chatButton"]', { timeout: 40000 })
            .should('be.visible')
            .click();
    }

    clickOnFlightBtnFromChat() {
        cy.get('[data-cy="chatMessageboxFlightButton"]')
            .should('be.visible')
            .click();
    }

    clickOnHotelBtnFromChat() {
        cy.get('[data-cy="chatMessageboxHotelButton"]')
            .should('be.visible')
            .click();
    }

    clickOnTrainBtnFromChat() {
        cy.get('[data-cy="chatMessageboxTrainButton"]')
            .should('be.visible')
            .click();
    }

    clickOnCarBtnFromChat() {
        cy.get('[data-cy="chatMessageboxCarButton"]')
            .should('be.visible')
            .click();
    }

    addTraveler(traveler) {
        cy.get('[data-cy="selectTraveler"]')
            .click()
            .type(traveler);
        cy.intercept('**/api/users/company-traveller*')
            .as('traveler');
        cy.wait('@traveler');
        cy.get('ng-dropdown-panel')
            .should('be.visible')
            .contains(traveler)
            .click();
        cy.get('[data-cy="selectTraveler"]')
            .should('contain', traveler);
    }

    clickOnPersonalSettings() {
        cy.get('[data-cy="personalSettingsButton"]')
            .should('be.visible')
            .click();
    }

    checkChatMessage(sentBy, chatMessage) {
        common.clickOnChatBtn();
        cy.get('app-chat-message-bubble')
            .last()
            .scrollIntoView()
            .should('be.visible');
        cy.get('[class="sent-by-' + sentBy + ' ng-star-inserted"]').then(($elements) => {
            const lastTwoElements = $elements.slice(-2);
            expect(lastTwoElements).to.contain(chatMessage);
        })
    }

    cancelTrip() {
        cy.intercept('**/api/users/current/reports/itinerary?interval=UPCOMING*')
            .as('itinerary');
        this.clickOnDashboard();
        this.clickOnNextTrips();
        this.cancelItineraries();
    }

    clickOnDashboard() {
        cy.get('[data-cy="dashboardButton"]')
            .should('be.visible')
            .click();
    }

    clickOnNextTrips() {
        cy.get('[data-cy="nextTripsButton"]')
            .should('be.visible')
            .click();
        cy.wait('@itinerary').then((itinerary) => {
            expect(itinerary.response.statusCode).to.equal(200);
        });
    }

    cancelItineraries() {
        cy.wait(500);
        cy.get('body').then($el => {
            const itinerary = $el.find('[data-cy="itineraryItem"]').length;
            if (itinerary) {
                cy.get('[data-cy="itineraryItem"]').then(($els) => {
                    const elementsArray = Array.from($els);
                    if (elementsArray.length === 1) {
                        this.clickOnItinerary(0);
                        this.cancelBooking();
                    } else if (elementsArray.length === 2) {
                        this.clickOnItinerary(0);
                        this.cancelBooking();
                        this.clickOnDashboard();
                        this.clickOnNextTrips();
                        this.clickOnItinerary(1);
                        this.cancelBooking();
                    } else if (elementsArray.length === 3) {
                        this.clickOnItinerary(0);
                        this.cancelBooking();
                        this.clickOnDashboard();
                        this.clickOnNextTrips();
                        this.clickOnItinerary(1);
                        this.cancelBooking();
                        this.clickOnDashboard();
                        this.clickOnNextTrips();
                        this.clickOnItinerary(2);
                        this.cancelBooking();
                    }
                })
            }
        })
    }

    clickOnItinerary(index) {
        cy.intercept('**/bos-report-service/api/users/current/reports/itinerary/**')
            .as('itineraryReport');
        cy.get('[data-cy="itineraryItem"]')
            .eq(index)
            .should('be.visible')
            .click();
    }

    cancelBooking() {
        cy.wait('@itineraryReport').then((report) => {
            expect(report.response.statusCode).to.equal(200);
        })
        cy.get('body').then(el => {
            const cancellation = el.find('[type="warning"]').length;
            if (!cancellation) {
                cy.get('[data-cy="editTripButton"]')
                    .first()
                    .should('be.visible')
                    .click();
                cy.contains('button', 'Cancel booking')
                    .should('be.visible')
                    .click();
                cy.contains('button', 'Cancel booking')
                    .should('be.visible')
                    .click();
            }
        })
    }

}

export const common = new Common();
