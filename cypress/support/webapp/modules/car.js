import { datePicker } from './datePicker';
import { common } from "./common";
import { checkout } from "./checkout";

export class Car {

    requestCar(pickupLocation, comment, returnLocation, additionalTraveler) {
        cy.intercept('GET', '**/backend/api/users/company-traveller**').as('companyTraveller');
        common.clickOnChatBtn();
        cy.url().should('include', '/chat');
        common.clickOnCarBtnFromChat();
        cy.url().should('include', '/chat?search=car');
        this.selectPickupLocation(pickupLocation);
        if (typeof returnLocation !== "undefined") {
            this.selectReturnLocation(returnLocation);
        }
        datePicker.setPickupAndReturnDate();
        if (typeof additionalTraveler !== "undefined") {
            common.addTraveler(additionalTraveler);
        }
        this.addComment(comment);
        cy.wait('@companyTraveller').then((company) => {
            expect(company.response.statusCode).to.equal(200);
        });
        this.clickOnSearchCars();
    }

    selectPickupLocation(pickupLocation) {
        cy.get('app-instant-options-cars form')
            .find('[data-cy="instantOptionsCarsPickupLocationSelectElement"]')
            .click()
            .type(pickupLocation, { delay: 50 });
        cy.get('ng-dropdown-panel')
            .contains(pickupLocation)
            .should('be.visible')
            .click();
        cy.get('app-instant-options-cars form')
            .find('[data-cy="instantOptionsCarsPickupLocationSelectElement"]')
            .should('contain', pickupLocation);
    }

    selectReturnLocation(returnLocation) {
        this.clickOnEnableReturn();
        cy.get('app-instant-options-cars form')
            .find('[data-cy="instantOptionsCarsReturnLocationSelectElement"]')
            .click()
            .type(returnLocation, { delay: 50 });
        cy.get('ng-dropdown-panel')
            .contains(returnLocation)
            .should('be.visible')
            .click();
        cy.get('app-instant-options-cars form')
            .find('[data-cy="instantOptionsCarsReturnLocationSelectElement"]')
            .should('contain', returnLocation);
    }

    clickOnEnableReturn() {
        cy.get('[data-cy="instantOptionsCarsReturnToggle"]')
            .should('be.visible')
            .click();
    }

    addComment(comment) {
        cy.get('app-instant-options-cars form')
            .find('[data-cy="instantOptionsCarsAddComment"]')
            .clear()
            .type(comment);
    }

    clickOnSearchCars() {
        cy.get('[data-cy="forwardRequestButton"]')
            .should('be.visible')
            .click();
    }

    confirmOptionSendViaChat() {
        common.clickOnChatBtn();
        this.extendCarOption();
        checkout.setCardCVC();
        cy.intercept('**/backend/api/car/**').as('confirm');
        checkout.clickOnConfirmAndPay();
        cy.wait('@confirm').then((confirmation) => {
            expect(confirmation.response.statusCode).to.equal(200);
        });
    }

    extendCarOption() {
        cy.get('app-chat-car-option')
            .last()
            .scrollIntoView()
            .should('be.visible')
            .click().then(() => {
            cy.get('app-extended-option')
                .should('be.visible').within(() => {
                cy.contains('button', 'Select')
                    .should('be.visible')
                    .click();
            })
        })
    }
}

export const car = new Car();
