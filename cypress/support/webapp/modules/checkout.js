import 'cypress-iframe';
// import { teams } from "./teams";

const { DateTime } = require("luxon");
const today = DateTime.local();

export class CheckOut {

    checkOutConfirmation(approvalReason, customField, serviceType, cardHolderName) {
        cy.wait(1000);
        this.acceptTermsAndConditions();
        this.addApprovalReason(approvalReason);
        this.addProjectNumber(customField);
        this.performPaymentConfirmation(cardHolderName);
        // teams.verifyCardHolderName(cardHolderName);
        cy.intercept('**/backend/api/'+ serviceType +'/**').as('confirm');
        this.clickOnConfirmAndPay();
        cy.wait('@confirm');
        cy.url().should('include', '/chat');
    }

    acceptTermsAndConditions() {
        cy.get('app-checkout').then(($checkout) => {
            if ($checkout.find('[type="checkbox"]').length) {
                cy.get('[type="checkbox"]')
                    .click({
                        force: true,
                        multiple: true
                    });
            } else {
                return;
            }
        })
    }

    addApprovalReason(approvalReason) {
        cy.get('body').then(($body) => {
            if ($body.find(`[data-cy="checkoutApprovalReason"]`).length) {
                cy.get(`[data-cy="checkoutApprovalReason"]`)
                    .type(approvalReason);
            } else {
                return;
            }
        })
    }

    addProjectNumber(customField) {
        cy.get('body').then(($body) => {
            if ($body.find(`[data-cy="checkoutCustomField"]`).length) {
                cy.get(`[data-cy="checkoutCustomField"]`)
                    .type(customField);
            } else {
                return;
            }
        })
    }

    performPaymentConfirmation(cardHolderName) {
        cy.get('app-payment-method')
            .should('be.visible');
        cy.get('body').then(($body) => {
            const hasPaymentMethod = $body.find('app-payment-method').length;
            if (hasPaymentMethod) {
                const hasSelectedCard = $body.find('app-credit-card').length;
                if (hasSelectedCard) {
                    const hasCVCInputField = $body.find('.cvc-section-container:not(.d-none) #cvv').length;
                    if (hasCVCInputField) {
                        this.setCardCVC(0);
                    }
                } else {
                    this.addNewCard(cardHolderName);
                    this.setCardCVC(0);
                }
            }
        })
    }

    addNewCard(cardHolderName) {
        cy.get('[data-cy="add-card-button"]')
            .should('be.visible')
            .click();
        this.setTemporaryCardDetails(cardHolderName);
        cy.get('[data-cy="addNewCardSaveButton"]')
            .should('be.visible')
            .click();
    }

    setTemporaryCardDetails(cardHolderName) {
        this.setCardDetails(cardHolderName);
        this.uncheckSaveToProfile();
    }

    setCardDetails(cardHolderName) {
        cy.get('app-add-credit-card-dialog').within(() => {
            this.setVisaCardNumber();
            this.setCardHolderName(cardHolderName);
            this.setCardExpiryMonthDate(5);
            this.setCardExpiryYearDate(7);
        });
    }

    setVisaCardNumber() {
        cy.iframe()
            .find('input#field')
            .type('4242424242424242');
    }

    setCardHolderName(cardHolderName) {
        cy.get('[data-cy="cardHolderNameInputField"]')
            .should('be.visible')
            .type(cardHolderName);
    }

    setCardExpiryMonthDate(monthsToAdd) {
        const monthExp = (today.month + monthsToAdd) % 12 || 12;
        const formattedMonth = monthExp.toString().padStart(2, '0');
        cy.get('[data-cy="cardExpirationMonthSelectElement"]')
            .should('exist')
            .clear()
            .type(formattedMonth);
    }

    setCardExpiryYearDate(yearsToAdd) {
        const expirationYear = today.plus({ years: yearsToAdd }).year;
        const formattedYear = expirationYear.toString();
        cy.get('[data-cy="cardExpirationYearSelectElement"]')
            .should('exist')
            .clear()
            .type(formattedYear);
    }

    setCardCVC(inputIndex) {
        cy.frameLoaded('.cvc-section-container iframe');
        cy.iframe('.cvc-section-container iframe')
            .find('input')
            .should('be.visible')
            .type('123');
    }

    uncheckSaveToProfile() {
        cy.get('[data-cy="save-to-profile-checkbox"]')
            .should('be.visible')
            .click();
    }

    clickOnConfirmAndPay() {
        cy.get('[data-cy="checkoutConfirmBookingButton"]')
            .should('be.visible')
            .click();
    }
}

export const checkout = new CheckOut();
