import { datePicker } from './datePicker';
import { checkout } from './checkout';
import { common } from "./common";
import { serviceType } from "../constants/flight";
import { cardHolderName } from "../constants/card";

export class Flight {

    bookFlight(departure, arrival, flightType, approvalReason, projectNumber, additionalTraveler) {
        common.clickOnChatBtn();
        cy.url().should('include', '/chat');
        common.clickOnFlightBtnFromChat();
        cy.url().should('include', '/chat?search=flight');
        this.addFromAndToAirport(departure, arrival);
        this.setTravelDate(flightType);
        if (typeof additionalTraveler !== "undefined") {
            common.addTraveler(additionalTraveler);
        }
        cy.intercept('**/api/flight/search/outbounds*').as('searchFlights');
        this.clickOnSearchFlights();
        cy.wait('@searchFlights').then((flights) => {
            expect(flights.response.statusCode).to.equal(200);
        });
        cy.intercept('**/api/cards*').as('cards');
        cy.intercept('**/approval/summary*').as('summary');
        cy.intercept('**/error-summary*').as('errorSummary');
        this.selectOption(flightType, departure);
        cy.wait('@cards').then((cards) => {
            expect(cards.response.statusCode).to.equal(200);
        });
        cy.wait('@summary').then((summary) => {
            expect(summary.response.statusCode).to.equal(200);
        });
        cy.wait('@errorSummary').then((errorSummary) => {
            expect(errorSummary.response.statusCode).to.equal(200);
        });
        checkout.checkOutConfirmation(approvalReason, projectNumber, serviceType, cardHolderName.flightName);
    }

    addFromAndToAirport(departure, arrival) {
        this.addFromAirport(departure);
        this.addToAirport(arrival);
    }

    addFromAirport(airport) {
        cy.get('.departure-airport-select')
            .should('be.visible')
            .find('input')
            .clear()
            .type(airport);
        cy.intercept('/backend/api/airport/search*')
            .as('search');
        cy.wait('@search');
        cy.get('.ng-dropdown-panel-items')
            .should('be.visible')
            .contains(airport)
            .click();
    }

    addToAirport(airport) {
        cy.get('.destination-airport-select')
            .should('be.visible')
            .find('input')
            .clear()
            .type(airport);
        cy.wait('@search');
        cy.get('.ng-dropdown-panel-items')
            .should('be.visible')
            .contains(airport)
            .click();
    }

    clickOnSearchFlights() {
        cy.get('[data-cy="searchFlightsButton"]')
            .should('be.visible')
            .click();
    }

    selectOption(optionType, departure) {
        if (optionType == 'oneway') {
            this.selectDeparture(departure);
            this.selectFare();
        } else if (optionType == 'roundtrip') {
            this.selectDeparture();
            this.selectReturn();
            this.selectFare();
        }
    }

    selectDeparture(departure) {
        cy.intercept('**/api/flight/search/inbounds*')
            .as('inbounds');
        cy.intercept('**/api/flight/pricing/upsells*')
            .as('fare');
        if (departure === 'London') {
            cy.contains('button', 'Show more flights')
                .scrollIntoView()
                .should('be.visible')
                .click();
            cy.contains('mat-expansion-panel', 'EASYJET')
                .should('exist')
                .first()
                .find('button')
                .contains('Select')
                .should('be.visible')
                .click();
        } else {
            cy.get('app-flight-search-results')
                .find('mat-expansion-panel')
                .should('exist')
                .first()
                .find('button')
                .contains('Select')
                .should('be.visible')
                .click();
        }
    }

    selectReturn() {
        cy.wait('@inbounds');
        cy.intercept('**/api/flight/pricing/upsells*')
            .as('fare');
        cy.get('app-flight-search-results:visible')
            .eq(1)
            .find('mat-expansion-panel:visible')
            .should('exist')
            .find('button')
            .contains('Select')
            .should('be.visible')
            .click();
    }

    selectFare() {
        cy.wait('@fare').then((fare) => {
            expect(fare.response.statusCode).to.equal(200);
        });
        cy.get('app-flight-search-fares')
            .find('app-flight-search-fare')
            .should('exist')
            .first()
            .find('button')
            .contains('Select')
            .should('be.visible')
            .click();
    }

    setTravelDate(flightType) {
        if (flightType == 'oneway') {
            cy.get('mat-radio-button')
                .should('be.visible')
                .contains('One way')
                .click();
            datePicker.setOutboundDate();
        } else if (flightType == 'roundtrip') {
            cy.get('mat-radio-button')
                .should('be.visible')
                .contains('Round trip')
                .click();
            datePicker.setOutboundAndInboundDate();
        }
    }

    confirmOptionSendViaChat(approvalReason, projectNumber) {
        common.clickOnChatBtn();
        cy.intercept('**/api/cards*').as('cards');
        cy.intercept('**/approval/summary*').as('summary');
        cy.intercept('**/error-summary*').as('errorSummary');
        cy.get('app-chat-flight-option')
            .last()
            .scrollIntoView()
            .should('be.visible')
            .click();
        cy.get('app-extended-option')
            .should('be.visible').within(() => {
            cy.contains('button', 'Select')
                .should('be.visible')
                .click();
        });
        cy.wait('@cards').then((cards) => {
            expect(cards.response.statusCode).to.equal(200);
        });
        cy.wait('@summary').then((summary) => {
            expect(summary.response.statusCode).to.equal(200);
        });
        cy.wait('@errorSummary').then((errorSummary) => {
            expect(errorSummary.response.statusCode).to.equal(200);
        });
        checkout.checkOutConfirmation(approvalReason, projectNumber, serviceType, cardHolderName.hotelName);
    }
}

export const flight = new Flight();
