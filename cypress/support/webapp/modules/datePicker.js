const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const outboundDate = DateTime.local().plus({
    days: 25
});
const inboundDate = DateTime.local().plus({
    days: 26
});
const indexOutbound = outboundDate.month === month ? 0 : 1;
const indexInbound = inboundDate.month === month ? 0 : 1;

export class DatePicker {

    setOutboundDate(optionType) {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');

        // open date picker
        cy.get('[data-cy="outboundDatePicker"]')
            .click();

        if (outboundDate.month - month == 1 || outboundDate.year > today.year) {
            cy.get('.right > .btn > .ngb-dp-navigation-chevron')
                .click();
        }

        // set outbound day
        cy.get('ngb-datepicker-month')
            .eq(0)
            .find('div')
            .contains(outboundDay)
            .click();
        if (optionType == "onewayTrain") {
            cy.get('timepicker')
                .find('input')
                .eq(0)
                .clear()
                .type('07');

            cy.get('timepicker')
                .find('input')
                .eq(1)
                .clear()
                .type('00');
        }
    }

    setOutboundAndInboundDate(optionType) {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');
        let inboundDay = DateTime.fromISO(inboundDate).toFormat('d');

        // open date picker
        cy.get('[data-cy="outboundDatePicker"]')
            .click();

        // set outbound day
        cy.get('ngb-datepicker-month')
            .eq(indexOutbound)
            .find('div')
            .contains(outboundDay)
            .click();

        // set inbound day
        cy.get('ngb-datepicker-month')
            .eq(indexInbound)
            .find('div')
            .contains(inboundDay)
            .click();

        if (optionType == "roundtripTrain") {
            cy.get('timepicker')
                .eq(0)
                .find('input')
                .eq(0)
                .clear()
                .type('07');

            cy.get('timepicker')
                .eq(0)
                .find('input')
                .eq(1)
                .clear()
                .type('00');

            cy.get('timepicker')
                .eq(1)
                .find('input')
                .eq(0)
                .clear()
                .type('07');

            cy.get('timepicker')
                .eq(1)
                .find('input')
                .eq(1)
                .clear()
                .type('00');
        }

    }

    setCheckinAndCheckoutDate() {
        let checkinDay = DateTime.fromISO(outboundDate).toFormat('d');
        let inboundDay = DateTime.fromISO(inboundDate).toFormat('d');

        // open date picker
        cy.get('[data-cy="checkinDatePicker"]')
            .click();

        // set checkin day
        cy.get('ngb-datepicker-month')
            .eq(indexOutbound)
            .find('div')
            .contains(checkinDay)
            .click();

        // set checkout day
        cy.get('ngb-datepicker-month')
            .eq(indexInbound)
            .find('div')
            .contains(inboundDay)
            .click();
    }

    setPickupAndReturnDate() {
        let pickupDay = DateTime.fromISO(outboundDate).toFormat('d');
        let returnDay = DateTime.fromISO(inboundDate).toFormat('d');

        // open pickup date picker
        cy.get('[data-cy="pickupDatePicker"]')
            .click();

        // set pickup day
        cy.get('ngb-datepicker-month')
            .eq(indexOutbound)
            .find('div')
            .contains(pickupDay)
            .click();

        // set return day
        cy.get('ngb-datepicker-month')
            .eq(indexInbound)
            .find('div')
            .contains(returnDay)
            .click();
    }
}

export const datePicker = new DatePicker();
