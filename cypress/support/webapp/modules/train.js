import { common } from "./common";
import { serviceType, ticketType } from "../constants/train";
import { cardHolderName } from "../constants/card";
import { checkout } from './checkout';

const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const outboundDate = DateTime.local().plus({
    days: 7
});
const inboundDate = DateTime.local().plus({
    days: 12
});
export class Train {
    bookTrain(departure, arrival, bookingType, trainClass, fareType, approvalReason, projectNumber) {
        common.clickOnChatBtn();
        cy.url().should('include', '/chat');
        cy.intercept('GET', '**/backend/api/users/company-traveller**').as('companyTraveller');
        common.clickOnTrainBtnFromChat();
        cy.url().should('include', '/chat?search=train');
        cy.wait('@companyTraveller').then((company) => {
            expect(company.response.statusCode).to.equal(200);
        });
        this.selectFromToStations(departure, arrival);
        this.setTravelDate(bookingType);
        this.selectTravelClass(trainClass);
        cy.intercept('**/api/trains/availabilities').as('trainsSearch');
        this.clickOnSearchTrains();
        cy.wait('@trainsSearch').then((trains) => {
            expect(trains.response.statusCode).to.equal(200);
        });
        cy.intercept('**/cards*').as('cards');
        cy.intercept('**/error-summary*').as('errorSummary');
        this.selectOption(bookingType, fareType);
        cy.wait('@cards').then((cards) => {
            expect(cards.response.statusCode).to.equal(200);
        });
        cy.wait('@errorSummary').then((errorSummary) => {
            expect(errorSummary.response.statusCode).to.equal(200);
        });
        checkout.checkOutConfirmation(approvalReason, projectNumber, serviceType, cardHolderName.trainName);
    }

    selectFromToStations(departure, arrival) {
        this.selectStation('[data-cy="instantOptionsTrainsOutboundStationSelectElement"]', departure);
        this.selectStation('[data-cy="instantOptionsTrainsInboundStationSelectElement"]', arrival);
    }

    selectStation(fromToSelector, station) {
        cy.intercept('**/train-stations/_search*')
            .as('trainStations');
        cy.get(fromToSelector)
            .should('be.visible')
            .find('input')
            .clear()
            .type(station);
        cy.wait('@trainStations');
        cy.get('[role="listbox"]')
            .contains(station, { timeout: 15000 })
            .should('be.visible')
            .click();
        cy.get(fromToSelector)
            .find('input')
            .invoke('prop', 'value')
            .should('contain', station);
    }

    setTravelDate(bookingType) {
        if (bookingType == 'onewayTrain') {
            cy.get('mat-radio-button')
                .should('be.visible')
                .contains('One way')
                .click();
            this.setOutboundDay();
        } else if (bookingType == 'roundtripTrain') {
            cy.get('mat-radio-button')
                .should('be.visible')
                .contains('Round trip')
                .click();
            this.setOutboundDay();
            this.setInboundDay();
        }
    }

    setOutboundDay() {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');
        cy.get('[data-cy="outboundDatePicker"]')
            .should('be.visible')
            .click();
        if (outboundDate.month - month == 1 || outboundDate.year > today.year) {
            cy.get('.right > .btn > .ngb-dp-navigation-chevron')
                .click();
        }
        cy.get('ngb-datepicker-month')
            .eq(0)
            .find('div')
            .contains(outboundDay)
            .click();
        cy.get('[data-cy="outboundTimePicker"]')
            .should('be.visible')
            .clear()
            .type('07:00');
    }

    setInboundDay() {
        let inboundDay = DateTime.fromISO(inboundDate).toFormat('d');
        let currentDate = DateTime.now();
        let monthIndex = 0;
        if (DateTime.fromISO(inboundDate).month > currentDate.month) {
            monthIndex = 1;
        }
        cy.get('[data-cy="inboundDatePicker"]')
            .should('be.visible')
            .click();
        cy.get('ngb-datepicker-month')
            .eq(monthIndex)
            .find('div')
            .contains(inboundDay)
            .click();
        cy.get('[data-cy="inboundTimePicker"]')
            .should('be.visible')
            .clear()
            .type('07:00');
    }

    selectTravelClass(trainClass) {
        cy.get('[data-cy="travelClassSelect"]')
            .should('exist')
            .click();
        cy.get('mat-radio-button')
            .contains(trainClass)
            .click();
        cy.get('[data-cy="travelClassSelect"]')
            .should('contain', trainClass);
    }

    clickOnSearchTrains() {
        cy.get('[data-cy="searchTrainsButton"]')
            .should('be.enabled')
            .click();
    }

    selectOption(optionType, fareType) {
        if (optionType === 'onewayTrain') {
            this.selectOutwardTrain();
            this.selectOutwardFare(fareType);
            this.clickOnContinue();
            this.clickOnContinueToSummary();
        } else if (optionType === 'roundtripTrain') {
            this.selectOutwardTrain();
            cy.intercept('**/api/trains/availabilities').as('trainsAvailabilities');
            this.selectOutwardFare(fareType);
            this.clickOnContinue();
            cy.wait('@trainsAvailabilities').then((trains) => {
                expect(trains.response.statusCode).to.equal(200);
            });
            this.selectReturnTrain();
            this.selectReturnFare(fareType);
            this.clickOnContinue();
            this.clickOnContinueToSummary();
        }
    }

    selectOutwardTrain() {
        cy.get('[direction="outward"] app-train-search-results', { timeout: 60000 }).within(() => {
            cy.contains('[data-cy="trainOption"]', 'ICE')
                .first()
                .should('exist')
                .contains('Select')
                .click();
        })
    }

    selectReturnTrain() {
        cy.get('[direction="return"] app-train-search-results').within(() => {
            cy.contains('[data-cy="trainOption"]', 'ICE')
                .first()
                .should('be.visible')
                .contains('Select')
                .click();
        })
    }

    selectFirstFare() {
        cy.get('app-train-search-fares').last()
            .should('be.visible').within(() => {
            cy.get('[data-cy="faresOption"]')
                .first()
                .should('be.visible')
                .click();
        })
    }

    selectOutwardFare(fareType) {
        if (fareType === 'First') {
            this.selectFirstFare();
        } else {
            cy.get('[direction="outward"] [data-cy="faresOption"]')
                .contains(fareType)
                .should('be.visible')
                .click();
        }
    }

    selectReturnFare(fareType) {
        if (fareType === 'First') {
            this.selectFirstFare();
        } else {
            cy.get('[direction="return"] [data-cy="faresOption"]:contains(' + ticketType.sparpreis + ')')
                .last()
                .should('be.visible')
                .click();
        }
    }

    clickOnContinue() {
        cy.get('[data-cy="continueButton"]')
            .last()
            .should('be.visible')
            .click();
    }

    clickOnContinueToSummary() {
        cy.get('[data-cy="continueToSummary"]')
            .should('be.visible')
            .click();
    }

    confirmOptionSendViaChat(approvalReason, projectNumber) {
        common.clickOnChatBtn();
        cy.intercept('**/api/cards*').as('cards');
        cy.intercept('**/approval/summary*').as('summary');
        cy.intercept('**/error-summary*').as('errorSummary');
        cy.get('app-chat-train-option')
            .last()
            .scrollIntoView()
            .should('be.visible')
            .click();
        cy.get('app-extended-option')
            .should('be.visible').within(() => {
            cy.contains('button', 'Select')
                .should('be.visible')
                .click();
        });
        cy.wait('@cards').then((cards) => {
            expect(cards.response.statusCode).to.equal(200);
        });
        cy.wait('@summary').then((summary) => {
            expect(summary.response.statusCode).to.equal(200);
        });
        cy.wait('@errorSummary').then((errorSummary) => {
            expect(errorSummary.response.statusCode).to.equal(200);
        });
        checkout.checkOutConfirmation(approvalReason, projectNumber, serviceType, cardHolderName.trainName);
    }
}

export const train = new Train();
