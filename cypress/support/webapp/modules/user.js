const url = Cypress.env('webappUrl');

export class User {
    signIn(username, password) {
        cy.visit(url);
        cy.url({ timeout: 10000 })
            .should('include', '/auth/login')
        cy.get('[data-cy="loginEmailAddressInputField"]')
            .should('be.visible')
            .type(username);
        cy.get('[data-cy="loginPasswordInputField"]')
            .should('be.visible')
            .type(password);
        cy.get('[data-cy="loginButton"]')
            .should('be.visible')
            .click();
        cy.url({ timeout: 20000 })
            .should('include', '/trip-search' + '');
    }

    logOut() {
        cy.url().then((currentUrl) => {
            if (currentUrl === url + '/chat') {
                cy.get('[data-cy="logoutButton"]')
                    .should('be.visible')
                    .click();
            } else {
                cy.visit(url + '/chat');
                cy.get('[data-cy="logoutButton"]')
                    .should('be.visible')
                    .click();
            }
            return cy.url({ timeout: 20000 })
                .should('include', '/auth/login');
        })
    }
}

export const user = new User();
