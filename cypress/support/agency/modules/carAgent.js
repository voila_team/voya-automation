import { task } from "./task";
import { carModel } from "../constants/car";

const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const outboundDate = DateTime.local().plus({
    days: 7
});
const inboundDate = DateTime.local().plus({
    days: 12
});

export class AgentCar {
    addTaskDetails(addPickUpAddress) {
        task.clickOnContinue();
        cy.intercept('**/backend/api/car/task/**').as('taskStart');
        cy.wait('@taskStart').then((carTask) => {
            expect(carTask.response.statusCode).to.equal(200);
        })
        this.clickOnAdd();
        this.selectCarModel(carModel);
        this.selectProvider();
        this.setStartDate();
        this.setStopDate();
        this.addPickUpAddress(addPickUpAddress);
        this.addPrice();
        this.clickOnSave();
        task.sendOptions();
        task.assertTaskWasFinish();
    }

    clickOnAdd() {
        cy.get('[data-cy="addButton"]')
            .should('be.visible')
            .click();
    }

    selectCarModel(carModel) {
        cy.get('[data-cy="modelInput"]')
            .should('be.visible')
            .type(carModel, { delay: 50 });
        cy.get('[role="option"]')
            .contains(carModel)
            .should('be.visible')
            .click();
        cy.get('[data-cy="modelInput"]')
            .should('have.value', carModel + ' or similar');
    }

    selectProvider() {
        cy.get('[data-cy="selectProvider"]')
            .should('be.visible')
            .and('exist')
            .click();
        cy.contains('mat-option', 'Sixt')
            .should('be.visible')
            .click();
    }

    setStartDate() {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');

        cy.get('[data-cy="startDateInput"]')
            .should('be.visible')
            .click();
        if (outboundDate.month - month == 1 || outboundDate.year > today.year) {
            cy.get('.mat-calendar-next-button')
                .should('be.visible')
                .click();
        }
        cy.get('mat-calendar').within(() => {
            cy.get('[mat-calendar-body]')
                .find('div')
                .contains(outboundDay)
                .click();
        })
        cy.get('[data-cy="startTimeInput"]')
            .should('be.visible')
            .type('11:00');
    }

    setStopDate() {
        let inboundDay = DateTime.fromISO(inboundDate).toFormat('d');

        cy.get('[data-cy="stopDateInput"]')
            .should('be.visible')
            .click();
        if (inboundDate.month - month == 1 || inboundDate.year > today.year) {
            cy.get('.mat-calendar-next-button')
                .should('be.visible')
                .click();
        }
        cy.get('mat-calendar').within(() => {
            cy.get('[mat-calendar-body]')
                .find('div')
                .contains(inboundDay)
                .click();
        })
        cy.get('[data-cy="stopTimeInput"]')
            .should('be.visible')
            .type('11:00');
    }

    addPickUpAddress(pickUpAddress) {
        cy.get('[ng-reflect-placeholder="Address"]')
            .should('be.visible')
            .first()
            .type(pickUpAddress);
        cy.get('.mat-option-text')
            .contains(pickUpAddress)
            .should('be.visible')
            .click();
    }

    addPrice() {
        cy.get('[data-cy="totalPriceInput"]')
            .scrollIntoView()
            .should('be.visible')
            .type('2000');
    }

    clickOnSave() {
        cy.get('.mat-dialog-actions').within(() => {
            cy.get('[data-cy="saveButton"]')
                .scrollIntoView()
                .should('be.visible')
                .click();
        })
    }

    confirmUserSelection(referenceNumber, supplier) {
        cy.intercept('**/backend/api/chains/**').as('chains');
        this.openBookService();
        cy.wait('@chains').then((chains) => {
            expect(chains.response.statusCode).to.eq(200);
        })
        task.addSupplier(supplier);
        task.addBookingInformation(referenceNumber);
        task.finishTask();
    }

    openBookService() {
        cy.get('app-todo-group').eq(0).within(() => {
            cy.get('[data-cy="todoLabel"]')
                .last()
                .should('be.visible')
                .click();
        })
    }
}

export const agentCar = new AgentCar();
