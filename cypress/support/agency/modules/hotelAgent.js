import { task } from "./task";

const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const isLastDayOfMonth = (date) => {
    const lastDayOfMonth = DateTime.fromISO(date).daysInMonth;
    const day = DateTime.fromISO(date).day;
    return day === lastDayOfMonth;
};
const outboundDate = DateTime.local().plus({
    days: 7
});
const inboundDate = DateTime.local().plus({
    days: 8
});

export class AgentHotel {

    addTaskDetails(referenceNumber) {
        cy.intercept('**/backend/api/chains/**').as('chains');
        task.openBookService();
        cy.wait('@chains').then((chains) => {
            expect(chains.response.statusCode).to.eq(200);
        })
        task.addBookingInformation(referenceNumber);
        task.finishTask();
    }

    sendHotelOptions(location) {
        cy.intercept('**/api/profiles/v3?userId=**')
            .as('userProfile');
        task.clickOnContinue();
        cy.wait('@userProfile').then((profile) => {
            expect(profile.response.statusCode).to.equal(200);
        });
        this.addLocation(location);
        this.selectCheckInCheckOutDate();
        task.clickOnSearch();
        this.selectHotelOffers();
        task.sendOptions();
        task.assertTaskWasFinish();
    }

    addLocation(location) {
        cy.intercept('**/api/hotel/bookingcom*').as('hotels');
        cy.get('[data-cy="locationInput"]')
            .should('be.visible')
            .type(location);
        cy.wait('@hotels').then(() => {
            cy.get('.mat-option-text')
                .first()
                .should('be.visible')
                .click();
        })
    }

    selectCheckInCheckOutDate() {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');
        let inboundDay = DateTime.fromISO(inboundDate).toFormat('d');
        const isOutboundLastDay = isLastDayOfMonth(outboundDate);
        let nextButtonClicked = false;

        cy.get('[data-cy="checkinInputField"]')
            .should('be.visible')
            .click();
        if (outboundDate.month - month == 1 || outboundDate.year > today.year) {
            if (!nextButtonClicked) {
                cy.get('.mat-calendar-next-button')
                    .should('be.visible')
                    .click();
                nextButtonClicked = true;
            }
        }
        cy.get('mat-calendar').within(() => {
            cy.get('[mat-calendar-body]')
                .find('div')
                .contains(outboundDay)
                .click();
        })
        cy.wait(500);
        cy.get('body').then($el => {
            const calendar = $el.find('mat-calendar');
            if (!calendar.is(':visible')) {
                cy.get('[data-cy="checkoutInputField"]')
                    .should('be.visible')
                    .click();
            }
        });
        if (
            (inboundDate.month - month === 1 && !nextButtonClicked && !isOutboundLastDay) ||
            (isOutboundLastDay && inboundDate.year > today.year && !nextButtonClicked)
        ) {
            cy.get('.mat-calendar-next-button')
                .last()
                .should('be.visible')
                .click();
        }
        cy.get('mat-calendar').last().within(() => {
            cy.get('[mat-calendar-body]')
                .find('div')
                .contains(inboundDay)
                .click();
        })
    }

    selectHotelOffers() {
        this.selectHotel();
        cy.get('[data-cy="addToBasketButton"]')
            .first()
            .should('be.visible')
            .click();
        this.selectHotel();
        cy.get('[data-cy="addToBasketButton"]')
            .last()
            .scrollIntoView()
            .should('be.visible')
            .click();
    }

    selectHotel() {
        cy.intercept('**/api/hotel/all-offers/**').as('hotelOffers');
        cy.contains('[data-cy="hotelResult"]', 'Mercure Bucharest Unirii')
            .first().within(() => {
            cy.get('[data-cy="showOffersButton"]')
                .should('be.visible')
                .click();
        })
        cy.wait('@hotelOffers').then((offers) => {
            expect(offers.response.statusCode).to.equal(200);
        });
    }

}

export const agentHotel = new AgentHotel();
