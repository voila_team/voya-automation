import { comment, tag } from "../constants/todo";
import { task } from "./task";

const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const travelDate = DateTime.local().plus({ days: 7 });
export class ToDo {
    openToDo() {
        cy.intercept('**/backend/api/todo').as('todo');
        cy.intercept('**/backend/api/tasks**').as('tasks');
        cy.wait('@tasks').then((tasks) => {
            expect(tasks.response.statusCode).to.equal(200);
        })
        task.closeTask();
        cy.get('body').then($el => {
            const todo = $el.find('[data-cy="todoGroup"]').length;
            if (!todo) {
                cy.reload();
                cy.wait('@tasks').then((tasks) => {
                    expect(tasks.response.statusCode).to.equal(200);
                })
                task.closeTask();
            }
        })
        cy.get('body').then($el => {
            const task = $el.find('[data-cy="taskMenuButton"]').length;
            if (task) {
                cy.get('[data-cy="taskMenuButton"]')
                    .should('be.visible')
                    .click();
                cy.get('[data-cy="cancelTaskButton"]')
                    .should('be.visible')
                    .click();
                cy.get('app-dialog-confirm-cancel-task').within(() => {
                    cy.get('[data-cy="confirmCancelTaskButton"]')
                        .should('exist')
                        .click();
                })
                cy.get('[data-cy="todoGroup"]').eq(0).within(() => {
                    cy.get('[data-cy="todoLabel"]')
                        .should('exist')
                        .last()
                        .scrollIntoView()
                        .should('be.visible')
                        .click();
                    cy.wait('@tasks');
                    cy.wait('@todo').then((todo) => {
                        expect(todo.response.statusCode).to.equal(200);
                    })
                    cy.wait(1000);
                })
                cy.get('body').then(el => {
                    const closeTask = el.find('.task-finish-btn-container:contains("Canceled")').length;
                    if (closeTask) {
                        cy.get('[ng-reflect-message="Close"]')
                            .should('exist')
                            .click();
                        cy.get('[data-cy="todoGroup"]').eq(0).within(() => {
                            cy.get('[data-cy="todoLabel"]')
                                .should('exist')
                                .last()
                                .scrollIntoView()
                                .should('be.visible')
                                .click();
                        })
                    }
                })
            } else {
                cy.get('[data-cy="todoGroup"]').eq(0).within(() => {
                    cy.get('[data-cy="todoLabel"]')
                        .should('exist')
                        .last()
                        .scrollIntoView()
                        .should('be.visible')
                        .click();
                    cy.wait('@todo').then((todo) => {
                        expect(todo.response.statusCode).to.equal(200);
                    })
                })
            }
        })
    }

    deleteTodos() {
        cy.get('body').then($el => {
            const toDo = $el.find('[data-cy="todoLabel"]').length;
            if(toDo > 0){
                cy.intercept('DELETE', '**/backend/api/todo/**')
                    .as('del');
                cy.get('[data-cy="todoLabel"]').each(($el) => {
                    if($el.length > 0) {
                        cy.get('app-todo-item').first()
                            .within(() => {
                                cy.get('[data-cy="todoLabel"]')
                                    .trigger('mouseover');
                                cy.get('button')
                                    .last()
                                    .click();
                            }).then(() => {
                            cy.get('.mat-dialog-container').within(() => {
                                cy.get('[data-cy="deleteButton"]')
                                    .should('be.visible')
                                    .click();
                                cy.get('[data-cy="confirmDeleteButton"]')
                                    .should('be.visible')
                                    .click();
                                cy.wait('@del').then((del) => {
                                    expect(del.response.statusCode).to.eq(200);
                                })
                                cy.reload();
                            })
                        })
                    }
                })
            }
        })
    }

    createToDo(serviceType) {
        cy.intercept('**/api/profiles/**').as('userProfile');
        cy.wait('@userProfile').then((profile) => {
            expect(profile.response.statusCode).to.eq(200);
        })
        task.closeTask();
        this.clickOnCreateToDo();
        this.addComment();
        this.selectTravelDate();
        this.selectServiceType(serviceType);
        this.addTag();
        this.clickOnCreateTodo();
        cy.reload();
    }

    clickOnCreateToDo() {
        cy.get('[data-cy="createTodoButton"]')
            .should('be.visible')
            .click()
    }

    selectTravelDate() {
        let travelDay = DateTime.fromISO(travelDate).toFormat('d');
        cy.get('[data-cy="travelDate"]')
            .should('be.visible')
            .click();
        if (travelDate.month - month == 1 || travelDate.year > today.year) {
            cy.get('.mat-calendar-next-button')
                .should('be.visible')
                .click();
        }
        cy.get('.mat-calendar-body')
            .find('div')
            .contains(travelDay)
            .click();
    }

    selectServiceType(serviceType) {
        cy.get('[data-cy="' + serviceType +'TypeButton"]')
            .should('be.visible')
            .click();
    }

    addComment() {
        cy.get('[data-cy="commentInputField"]')
            .should('be.visible')
            .type(comment);
    }

    addTag() {
        cy.get('[data-cy="tagInputField"]')
            .should('be.visible')
            .type(tag + '{enter}');
    }

    clickOnCreateTodo() {
        cy.get('[data-cy="createTodoButton"]')
            .should('be.visible')
            .click();
    }

    checkBookingAutomation(bookingType) {
        cy.intercept('**/backend/api/tasks**').as('tasks');
        cy.wait('@tasks').then((tasks) => {
            expect(tasks.response.statusCode).to.eq(200);
        })
        cy.wait(1000);
        cy.get('body').then(el => {
            const closeTask = el.find('.task-finish-btn-container').length;
            if (closeTask) {
                cy.get('[ng-reflect-message="Close"]')
                    .should('exist')
                    .click();
            }
        })
        cy.get('[data-cy="todoGroup"]')
            .eq(0).find('app-todo-item')
            .first().then(($el) => {
                const doubleBooking = $el.find('[data-cy="tagName"]:contains("Check double booking")').length;
                if (doubleBooking) {
                    cy.get('[data-cy="todoGroup"]').eq(0)
                        .find('app-todo-item')
                        .should('contain', 'Check double booking')
                        .find('[data-cy="todoLabel"]')
                        .click();
                    cy.get('[data-cy="confirmAndContinueButton"]', { timeout: 30000 })
                        .scrollIntoView()
                        .should('be.visible')
                        .and('be.enabled')
                        .click()
                        .wait(500);
                    if (bookingType === 'Hotel Automation') {
                        task.finishTask();
                    } else {
                        cy.get('[ng-reflect-message="Close"]')
                            .should('exist')
                            .click();
                        cy.get('[data-cy="todoGroup"]').eq(0).within(() => {
                            cy.get('app-todo-item')
                                .first().within(() => {
                                cy.get('[data-cy="tagName"]', { timeout: 120000 })
                                    .should('contain', 'Automation success');
                            })
                        })
                    }
                } else {
                    cy.get('[data-cy="todoGroup"]').eq(0).within(() => {
                        cy.get('app-todo-item')
                            .first().within(() => {
                            cy.get('[data-cy="tagName"]')
                                .should('contain', 'Automatable');
                            cy.get('[data-cy="tagName"]', { timeout: 120000 })
                                .should('contain', 'Automation success');
                        })
                    })
                }
        })
    }
}

export const toDo = new ToDo();
