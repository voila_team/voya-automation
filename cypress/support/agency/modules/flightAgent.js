import { task } from "./task";
const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const outboundDate = DateTime.local().plus({
    days: 20
});
export class AgentFlight {

    addTaskDetails(referenceNumber, ticketNumber) {
        cy.intercept('**/backend/api/chains/**').as('chains');
        task.openBookService();
        cy.wait('@chains').then((chains) => {
            expect(chains.response.statusCode).to.eq(200);
        })
        task.addBookingInformation(referenceNumber, ticketNumber);
        task.finishTask();
    }

    sendFlightOptions(departure, destination, searchType) {
        cy.intercept('**/api/profiles/v3?userId=**')
            .as('userProfile');
        task.clickOnContinue();
        cy.wait('@userProfile').then((profile) => {
            expect(profile.response.statusCode).to.equal(200);
        });
        this.selectDepartureAirport(departure);
        this.selectDestinationAirport(destination);
        this.selectSearchType(searchType);
        this.selectDepartureDate();
        this.selectDepartureTime();
        this.uncheckNonStop();
        cy.intercept('**/flight/search/outbounds**')
            .as('flightOptions');
        task.clickOnSearch();
        cy.wait('@flightOptions').then((flights) => {
            expect(flights.response.statusCode).to.equal(200);
        })
        this.selectOutboundFlight(departure);
        this.selectFare(0);
        this.selectFare(1);
        task.sendOptions();
        task.assertTaskWasFinish();
    }

    selectDepartureAirport(departureAirport) {
        cy.intercept('**/api/airport/search**')
            .as('airports');
        cy.get('[data-cy="departureAirportInput"]')
            .should('be.visible')
            .wait(500);
        cy.get('[data-cy="departureAirportInput"]')
            .should('exist')
            .find('input')
            .should('be.visible')
            .type(departureAirport);
        cy.wait('@airports').then((airports) => {
            expect(airports.response.statusCode).to.equal(200);
        }).then(() => {
            cy.get('[role="option"]')
                .should('be.visible')
                .contains(departureAirport)
                .click();
        })
    }

    selectDestinationAirport(destinationAirport) {
        cy.get('[data-cy="destinationAirportInput"]')
            .should('be.visible')
            .type(destinationAirport);
        cy.wait('@airports').then((airports) => {
            expect(airports.response.statusCode).to.equal(200);
        }).then(() => {
            cy.get('[role="option"]')
                .should('be.visible')
                .contains(destinationAirport)
                .click();
        })
    }

    selectSearchType(searchType) {
        cy.get('[data-cy="searchType"]')
            .contains(searchType)
            .should('be.visible')
            .click();
    }

    selectDepartureDate() {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');

        cy.get('[data-cy="departureDate"]')
            .should('be.visible')
            .click();
        if (outboundDate.month - month == 1 || outboundDate.year > today.year) {
            cy.get('.mat-calendar-next-button')
                .should('be.visible')
                .click();
        }
        cy.get('mat-calendar').within(() => {
            cy.get('[mat-calendar-body]')
                .find('div')
                .contains(outboundDay)
                .click();
        })
    }

    selectDepartureTime() {
        cy.get('[data-cy="departureTimeInput"]')
            .should('be.visible')
            .clear()
            .type('08:00');
    }

    uncheckNonStop() {
        cy.get('[data-cy="nonStopCheckbox"]')
            .should('be.visible')
            .click();
    }

    selectOutboundFlight(departure) {
        if (departure === 'London') {
            cy.contains('[role="button"]', 'more flight')
                .scrollIntoView()
                .should('be.visible')
                .click();
            cy.contains('[data-cy="flightOption"]', 'EASYJET')
                .first()
                .scrollIntoView()
                .should('be.visible')
                .find('button')
                .should('be.enabled')
                .click();
        } else {
            cy.get('[data-cy="flightOption"]')
                .first()
                .should('be.visible')
                .find('button')
                .should('be.enabled')
                .click();
        }
    }

    selectFare(index) {
        cy.get('[data-cy="addOptionButton"]', { timeout: 40000 })
            .eq(index)
            .scrollIntoView()
            .should('be.visible')
            .click();
    }
}

export const agentFlight = new AgentFlight();
