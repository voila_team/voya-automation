export class Task {

    clickOnContinue() {
        cy.get('[data-cy="continueButton"]')
            .should('exist')
            .click();
    }

    closeTask() {
        cy.wait(500);
        cy.get('body').then(($el) => {
            let closeTask = $el.find('[ng-reflect-message="Close"]').length;
            if (closeTask) {
                cy.get('[ng-reflect-message="Close"]')
                    .should('be.visible')
                    .click();
            }
        })
    }

    sendOptions() {
        cy.get('[data-cy="sendOptionsButton"]')
            .should('be.visible')
            .click();
        cy.intercept('**/backend/api/tasks/**').as('optionsSend');
        cy.get('.mat-dialog-actions').within(() => {
            cy.get('[data-cy="sendButton"]')
                .should('be.visible')
                .click();
        })
        cy.wait('@optionsSend').then((options) => {
            expect(options.response.statusCode).to.equal(200);
        });
    }

    assertTaskWasFinish() {
        cy.get('#toast-container > .ng-trigger')
            .should('exist')
            .and('contain', 'The task was finished successfully');
    }

    openBookService() {
        cy.get('app-todo-group').eq(0).within(() => {
            cy.get('[data-cy="todoLabel"]')
                .last()
                .should('be.visible')
                .click();
        })
    }

    addBookingInformation(referenceNumber, ticketNumber) {
        cy.contains('[role="tab"]', 'Custom fields')
            .scrollIntoView()
            .should('be.visible')
            .click();
        cy.wait(2000);
        cy.contains('[role="tab"]', 'Booking information')
            .scrollIntoView()
            .should('be.visible')
            .click();
        cy.get('[data-cy="bookingReferenceInput"]')
            .should('be.visible')
            .type(referenceNumber, { force: true, delay: 50 });
        cy.get('body').then($el => {
            const ticket = $el.find('[data-cy="ticketNumberInput"]').length;
            if (ticket) {
                cy.get('[data-cy="ticketNumberInput"]')
                    .should('be.visible')
                    .type(ticketNumber);
            }
        })
    }

    addSupplier(supplier) {
        cy.get('[data-cy="supplierInput"]')
            .scrollIntoView()
            .should('be.visible')
            .click().then(() => {
                cy.get('[role="listbox"]')
                    .last()
                    .contains(supplier)
                    .scrollIntoView()
                    .should('be.visible')
                    .click();
        })
        cy.get('[data-placeholder="Supplier"]')
            .invoke('prop', 'value')
            .should('contain', supplier);
    }

    finishTask() {
        cy.intercept('**/backend/api/tasks/**/finish').as('finishTask');
        cy.get('[data-cy="finishTaskButton"]')
            .should('be.visible')
            .click();
        cy.get('app-dialog-confirm-finish')
            .should('be.visible').within(() => {
            cy.get('[data-cy="confirmFinishButton"]')
                .should('be.visible')
                .click();
        })
        cy.wait('@finishTask').then((task) => {
            expect(task.response.statusCode).to.equal(200);
        })
        cy.get('.chat-message-bubble', { timeout: 20000 }).then(($el) => {
            const chatMessage = $el.slice(-2);
            expect(chatMessage).to.contain('reference number');
        })
    }

    cancelTasks() {
        cy.intercept('**/api/profiles/**').as('userProfile');
        cy.wait('@userProfile').then((profile) => {
            expect(profile.response.statusCode).to.eq(200);
        })
        cy.get('body').then($el => {
            const task = $el.find('[data-cy="taskButton"]').length;
            if (task > 0) {
                cy.intercept('PATCH', '**/backend/api/tasks/**/cancel')
                    .as('cancelTask');
                cy.get('[data-cy="taskButton"]').each(($el) => {
                    if ($el.length > 0) {
                        cy.get('[data-cy="taskButton"]')
                            .last()
                            .click().then(() => {
                            cy.get('[data-cy="taskMenuButton"]')
                                .should('be.visible')
                                .click();
                            cy.get('[data-cy="cancelTaskButton"]')
                                .should('be.visible')
                                .click();
                            cy.get('app-dialog-confirm-cancel-task').within(() => {
                                cy.get('[data-cy="confirmCancelTaskButton"]')
                                    .should('exist')
                                    .click();
                                cy.wait('@cancelTask').then((cancel) => {
                                    expect(cancel.response.statusCode).to.eq(200);
                                })
                            })
                        })
                    }
                })
            }
        })
    }

    cancelTrip() {
        cy.intercept('**/backend/api/tasks/**/finish')
            .as('finishTask');
        cy.get('[data-cy="todoGroup"]').eq(0).then($el => {
            const cancelTrip = $el.find('[data-cy="todoLabel"]:contains("Cancel"):contains("arrow_forward")').length;
            if (cancelTrip > 0) {
                cy.get('[data-cy="todoGroup"]')
                    .eq(0).find('[data-cy="todoLabel"]:contains("Cancel"):contains("arrow_forward")').each(($el) => {
                    if ($el.length > 0) {
                        cy.get('[data-cy="todoLabel"]:contains("Cancel"):contains("arrow_forward")')
                            .first()
                            .click().then(() => {
                                cy.get('[data-cy="grossAmountInput"]')
                                    .scrollIntoView()
                                    .should('be.visible')
                                    .type('200', { delay: 500 });
                                this.finishTask();
                                cy.wait('@finishTask').then((task) => {
                                    expect(task.response.statusCode).to.equal(200);
                                })
                        })
                    }
                })
            }
        })
    }

    clickOnSearch() {
        cy.get('[data-cy="searchButton"]')
            .should('be.visible')
            .click();
    }

    clickOnAddOption() {
        cy.get('[data-cy="addOptionButton"]')
            .should('be.visible')
            .click();
    }

    clickOnChangeButton() {
        cy.get('[data-cy="changeButton"]')
            .should('be.visible')
            .click();
    }

    addTraveler(traveler) {
        cy.intercept('**/api/users/company-traveller*')
            .as('traveler');
        cy.get('[data-cy="searchTravelerInput"]')
            .should('be.visible')
            .type(traveler);
        cy.wait('@traveler').then((traveler) => {
            expect(traveler.response.statusCode).to.equal(200);
        })
        cy.get('[role="option"]')
            .last()
            .contains(traveler)
            .should('be.visible')
            .click();
        cy.get('app-travellers-initialize')
            .should('contain', traveler);
    }
}

export const task = new Task();
