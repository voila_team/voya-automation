const url = Cypress.env('agencyUrl');

export class Agent {
    signIn(adminUser, adminPassword) {
        cy.visit(url);
        cy.url()
            .should('include', '/login')
        cy.get('[data-cy="loginEmailAddressInputField"]')
            .should('be.visible')
            .type(adminUser);
        cy.get('[data-cy="loginPasswordInputField"]')
            .should('be.visible')
            .type(adminPassword);
        cy.get('[data-cy="loginButton"]')
            .should('be.visible')
            .click();
    }

    openUserSession(userEmail) {
        cy.contains('[data-cy="sidenav"]', 'Users')
            .should('be.visible')
            .click();
        cy.get('[data-cy="searchInput"]')
            .should('be.visible')
            .type(userEmail + '{enter}').then(() => {
                cy.get('tr')
                    .should('have.length.lessThan', 3);
        })
        cy.get('tr')
            .should('contain', userEmail)
            .find('td a')
            .first()
            .should('be.visible')
            .invoke('removeAttr', 'target')
            .click();
    }

    logOut() {
        cy.visit(url);
        cy.url().as('url');
        cy.get('@url').then($url => {
            const url = $url.toString();
            if(url.includes('http://localhost:4201/#/login')) {
                cy.log('You have been successfully logged out');
                cy.url().should('include', '/login');
            } else {
                cy.get('[data-cy="arrowDownButton"]')
                    .should('be.visible')
                    .click();
                cy.contains('.menu-group', ' Logout')
                    .should('be.visible')
                    .click();
                cy.url().should('include', '/login');
            }
        })
    }

    getSessionNumber() {
        cy.url().then(url => {
            const regex = /session\/(\d+)/;
            const match = url.match(regex);
            const sessionNumber = match ? match[1] : null;
            Cypress.env('sessionNumber', sessionNumber);
            cy.log(`The number is ${sessionNumber}`);
        });
    }
}

export const agent = new Agent();
