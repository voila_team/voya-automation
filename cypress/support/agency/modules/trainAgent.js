import { task } from "./task";
import { inbound, outward, travelClass } from "../constants/train";

const { DateTime } = require("luxon");
const today = DateTime.local();
const month = today.month;
const outboundDate = DateTime.local().plus({
    days: 7
});

export class AgentTrain {
    addTaskDetails(referenceNumber, supplier) {
        cy.intercept('**/backend/api/chains/**').as('chains');
        task.openBookService();
        cy.wait('@chains').then((chains) => {
            expect(chains.response.statusCode).to.eq(200);
        })
        task.addBookingInformation(referenceNumber);
        task.addSupplier(supplier);
        task.finishTask();
    }

    sendTrainOptions() {
        cy.intercept('**/api/profiles/v3?userId=**')
            .as('userProfile');
        task.clickOnContinue();
        cy.wait('@userProfile').then((profile) => {
            expect(profile.response.statusCode).to.equal(200);
        });
        this.selectOutwardStation(outward);
        this.selectInboundStation(inbound);
        this.selectOutwardDate();
        this.selectOutwardTime();
        this.selectTravelClass(travelClass.secondClass);
        cy.intercept('**/tp/api/trains/availabilities')
            .as('trainsAvailabilities');
        task.clickOnSearch();
        cy.wait('@trainsAvailabilities').then((results) => {
            expect(results.response.statusCode).to.equal(200);
        })
        this.selectOutwardTrain();
        this.selectOutwardFare();
        this.addSeatReservation();
        task.clickOnAddOption();
        task.clickOnChangeButton();
        this.selectSecondTrainOption();
        this.selectSecondFareOption();
        task.clickOnAddOption();
        task.sendOptions();
        task.assertTaskWasFinish();
    }

    selectOutwardStation(trainStation) {
        cy.intercept('**/api/reports/train-stations/_search')
            .as('trainStations');
        cy.get('[data-cy="outwardInput"]')
            .should('be.visible')
            .type(trainStation);
        cy.wait('@trainStations').then((stations) => {
            expect(stations.response.statusCode).to.equal(200);
        }).then(() => {
            cy.get('[role="listbox"]')
                .should('exist')
                .contains(trainStation)
                .click();
        })
    }

    selectInboundStation(trainStation) {
        cy.get('[data-cy="inboundInput"]')
            .should('be.visible')
            .type(trainStation);
        cy.wait('@trainStations').then((stations) => {
            expect(stations.response.statusCode).to.equal(200);
        }).then(() => {
            cy.get('[role="listbox"]')
                .should('exist')
                .contains(trainStation)
                .click();
        })
    }

    selectOutwardDate() {
        let outboundDay = DateTime.fromISO(outboundDate).toFormat('d');

        cy.get('[data-cy="outwardDate"]')
            .should('be.visible')
            .click();
        if (outboundDate.month - month == 1 || outboundDate.year > today.year) {
            cy.get('.mat-calendar-next-button')
                .should('be.visible')
                .click();
        }
        cy.get('mat-calendar').within(() => {
            cy.get('[mat-calendar-body]')
                .find('div')
                .contains(outboundDay)
                .click();
        })
    }

    selectOutwardTime() {
        cy.get('[data-cy="outwardTimeInput"]')
            .should('be.visible')
            .clear()
            .type('08:00');
    }

    selectTravelClass(travelClass) {
        cy.get('[data-cy="travelClass"]')
            .should('be.visible')
            .contains(travelClass)
            .click();
    }

    selectOutwardTrain() {
        cy.contains('[data-cy="trainOption"]', 'ICE')
            .should('be.visible')
            .click();
    }

    selectOutwardFare() {
        cy.get('[data-cy="trainFares"]')
            .first()
            .should('be.visible')
            .find('button')
            .first()
            .should('be.enabled')
            .click();
    }

    addSeatReservation() {
        cy.get('[data-cy="seatReservationCheckbox"]')
            .should('be.visible')
            .click();
    }

    selectSecondTrainOption() {
        cy.get('[data-cy="trainOption"]')
            .last()
            .should('be.visible')
            .click();
    }

    selectSecondFareOption() {
        cy.get('[data-cy="trainFares"]')
            .last()
            .should('be.visible')
            .find('button')
            .first()
            .should('be.enabled')
            .click();
    }
}

export const agentTrain = new AgentTrain();
