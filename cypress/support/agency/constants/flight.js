export const departure = {
 hamburg: 'Hamburg',
 london: 'London',
}
export const destination = {
 berlin: 'Berlin',
 paris: 'Paris',
}
export const searchType = {
 oneWay: 'One way ',
 roundTrip: 'Round trip ',
 multiStop: 'Multi stop ',
}
