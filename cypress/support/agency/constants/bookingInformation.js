export const referenceNumber = '998877';
export const ticketNumber = '112-23344556677';
export const bookingType = {
    hotel: 'Hotel Automation',
    flight: 'Flight Automation',
}
export const sentBy = {
    agent: 'others',
    user: 'me',
}

