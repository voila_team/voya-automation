export const outward = 'Hamburg Hbf';
export const inbound = 'Berlin Hbf';
export const travelClass = {
    firstClass: '1st class',
    secondClass: '2nd class',
}
