import { user } from "../../support/webapp/modules/user";
import { flight } from "../../support/webapp/modules/flight";
import { agent } from "../../support/agency/modules/agentUser";
import { departure, destination, oneway } from '../../support/webapp/constants/flight';
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { referenceNumber, sentBy, ticketNumber } from "../../support/agency/constants/bookingInformation";
import { common } from "../../support/webapp/modules/common";
import { agentFlight } from "../../support/agency/modules/flightAgent";

const username = Cypress.env('userForFlightCrossOrigin');
const agentUser = Cypress.env('agentUserForFlightBooking');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe.skip('Flight Booking', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Book One-way flight -> London - Paris - Webapp', () => {
        cy.visit(url);
        flight.bookFlight(departure.london, destination.paris, oneway, approvalReason, projectNumber);
    });

    it('Confirm selected option - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentFlight.addTaskDetails(referenceNumber, ticketNumber);
    });

    it('Check that the reference number was received - Webapp', () => {
        cy.visit(url);
        common.checkChatMessage(sentBy.agent, referenceNumber);
    });
});
