import { user } from "../../support/webapp/modules/user";
import { agent } from "../../support/agency/modules/agentUser";
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { train } from "../../support/webapp/modules/train";
import { outward, inbound, oneWay, secondClass, seatReservation, ticketType } from "../../support/webapp/constants/train";
import { task } from "../../support/agency/modules/task";
import { referenceNumber, sentBy } from "../../support/agency/constants/bookingInformation";
import { supplier } from "../../support/agency/constants/task";
import { common } from "../../support/webapp/modules/common";
import { agentTrain } from "../../support/agency/modules/trainAgent";

const username = Cypress.env('userForTrainCrossOrigin');
const agentUser = Cypress.env('agentUserForTrainBooking');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe('Express train', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Book One-way train -> Hamburg - Berlin | 2nd Clas - Webapp', () => {
        cy.visit(url);
        train.bookTrain(outward.hamburg, inbound.berlin, oneWay, secondClass, seatReservation.no, ticketType.first, approvalReason, projectNumber);
    });

    it('Confirm selected option - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentTrain.addTaskDetails(referenceNumber, supplier.bahnOnline);
    });

    it('Check that the reference number was received - Webapp', () => {
        cy.visit(url);
        common.checkChatMessage(sentBy.agent, referenceNumber);
    });
});
