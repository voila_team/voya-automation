import { user } from "../../support/webapp/modules/user";
import { agent } from "../../support/agency/modules/agentUser";
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { referenceNumber, sentBy } from "../../support/agency/constants/bookingInformation";
import { common } from "../../support/webapp/modules/common";
import { hotel } from "../../support/webapp/modules/hotel";
import { hotelType, hotelName } from "../../support/webapp/constants/hotel";
import { agentHotel } from "../../support/agency/modules/hotelAgent";

const username = Cypress.env('userForHotelCrossOrigin');
const agentUser = Cypress.env('agentUserForHotelBooking');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe('Express hotel', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Book Hotel -> Bucharest - Webapp', () => {
        cy.visit(url);
        hotel.bookHotel(hotelName.mercureBucharest, hotelType, approvalReason, projectNumber);
    });

    it('Confirm selected option - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentHotel.addTaskDetails(referenceNumber);
    });

    it('Check that the reference number was received - Webapp', () => {
        cy.visit(url);
        common.checkChatMessage(sentBy.agent, referenceNumber);
    });
});
