import { user } from "../../support/webapp/modules/user";
import { agent } from "../../support/agency/modules/agentUser";
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { referenceNumber, sentBy, ticketNumber } from "../../support/agency/constants/bookingInformation";
import { common } from "../../support/webapp/modules/common";
import { serviceType } from "../../support/agency/constants/todo";
import { agentFlight } from "../../support/agency/modules/flightAgent";
import { flight } from "../../support/webapp/modules/flight";
import { departure, destination, searchType } from "../../support/agency/constants/flight";

const username = Cypress.env('userForFlightCrossOriginMultipleOptions');
const agentUser = Cypress.env('agentUserForFlightBookingMultipleOptions');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe.skip('Send flight options -> Confirm in webapp', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Send flight options - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.createToDo(serviceType.flight);
        toDo.openToDo();
        agentFlight.sendFlightOptions(departure.london, destination.paris, searchType.oneWay);
    });

    it('Confirm received option - Webapp', () => {
        cy.visit(url);
        flight.confirmOptionSendViaChat(approvalReason, projectNumber);
    });

    it('Book flight - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentFlight.addTaskDetails(referenceNumber, ticketNumber);
    });

    it('Check that the reference number was received - Webapp', () => {
        cy.visit(url);
        common.checkChatMessage(sentBy.agent, referenceNumber);
    });
});
