import { user } from "../../support/webapp/modules/user";
import { agent } from "../../support/agency/modules/agentUser";
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { common } from "../../support/webapp/modules/common";
import { hotel } from "../../support/webapp/modules/hotel";
import { areaType, hotelName } from "../../support/webapp/constants/hotel";
import { bookingType } from "../../support/agency/constants/bookingInformation";

const username = Cypress.env('userForHotelAutomation');
const agentUser = Cypress.env('agentUserForHotelAutomation');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe.skip('Hotel Booking', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Book Hotel -> Bucharest - Webapp', () => {
        cy.visit(url);
        hotel.bookHotel(hotelName.mercureBucharest, areaType, approvalReason, projectNumber);
    });

    it('Check hotel automation - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.checkBookingAutomation(bookingType.hotel);
    });

    it('Cancel the trip - Webapp', () => {
        cy.visit(url);
        common.cancelTrip();
    });

    it('Confirm trip cancellation - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTrip();
    });
});
