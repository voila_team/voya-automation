import { user } from "../../support/webapp/modules/user";
import { additionalComment, pickupLocation } from "../../support/webapp/constants/car";
import { agent } from "../../support/agency/modules/agentUser";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { car } from "../../support/webapp/modules/car";
import { referenceNumber, sentBy } from "../../support/agency/constants/bookingInformation";
import { supplier } from "../../support/agency/constants/task";
import { agentCar } from "../../support/agency/modules/carAgent";
import { common } from "../../support/webapp/modules/common";
import { message } from "../../support/agency/constants/car";

const username = Cypress.env('userForCarCrossOrigin');
const agentUser = Cypress.env('agentUserForCarBooking');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe('Car Booking', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Request Car - Webapp', () => {
        cy.visit(url);
        car.requestCar(pickupLocation, additionalComment);
        common.checkChatMessage(sentBy.user, message);
    });

    it('Find Car - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentCar.addTaskDetails(pickupLocation);
    });

    it('Confirm Received Option - Webapp', () => {
        cy.visit(url);
        car.confirmOptionSendViaChat();
    });

    it('Book Car - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        agentCar.confirmUserSelection(referenceNumber, supplier.sixt);
    });
});
