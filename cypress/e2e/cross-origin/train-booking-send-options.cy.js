import { user } from "../../support/webapp/modules/user";
import { agent } from "../../support/agency/modules/agentUser";
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { referenceNumber, sentBy } from "../../support/agency/constants/bookingInformation";
import { common } from "../../support/webapp/modules/common";
import { serviceType } from "../../support/agency/constants/todo";
import { agentTrain } from "../../support/agency/modules/trainAgent";
import { train } from "../../support/webapp/modules/train";
import { supplier } from "../../support/agency/constants/task";

const username = Cypress.env('userForTrainCrossOriginMultipleOptions');
const agentUser = Cypress.env('agentUserForTrainBookingMultipleOptions');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe('Send train options -> Confirm in webapp', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Send train options - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.createToDo(serviceType.train);
        toDo.openToDo();
        agentTrain.sendTrainOptions();
    });

    it('Confirm received option - Webapp', () => {
        cy.visit(url);
        train.confirmOptionSendViaChat(approvalReason, projectNumber);
    });

    it('Book train - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentTrain.addTaskDetails(referenceNumber, supplier.bahnOnline);
    });

    it('Check that the reference number was received - Webapp', () => {
        cy.visit(url);
        common.checkChatMessage(sentBy.agent, referenceNumber);
    });
});
