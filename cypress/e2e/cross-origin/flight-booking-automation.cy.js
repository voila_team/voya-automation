import { user } from "../../support/webapp/modules/user";
import { flight } from "../../support/webapp/modules/flight";
import { agent } from "../../support/agency/modules/agentUser";
import { departure, destination, oneway } from '../../support/webapp/constants/flight';
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { common } from "../../support/webapp/modules/common";
import { bookingType } from "../../support/agency/constants/bookingInformation";

const username = Cypress.env('userForFlightAutomation');
const agentUser = Cypress.env('agentUserForFlightAutomation');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe.skip('Flight Booking', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Book One-way flight -> Hamburg - Berlin - Webapp', () => {
        cy.visit(url);
        flight.bookFlight(departure.hamburg, destination.berlin, oneway, approvalReason, projectNumber);
    });

    it('Check flight automation - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.checkBookingAutomation(bookingType.flight);
    });

    it('Cancel the trip - Webapp', () => {
        cy.visit(url);
        common.cancelTrip();
    });

    it('Confirm trip cancellation - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTrip();
    });
});
