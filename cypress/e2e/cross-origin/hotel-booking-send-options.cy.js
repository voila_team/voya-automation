import { user } from "../../support/webapp/modules/user";
import { agent } from "../../support/agency/modules/agentUser";
import { approvalReason, projectNumber } from "../../support/webapp/constants/checkout";
import { toDo } from "../../support/agency/modules/todo";
import { task } from "../../support/agency/modules/task";
import { hotel } from "../../support/webapp/modules/hotel";
import { hotelName } from "../../support/webapp/constants/hotel";
import { referenceNumber, sentBy } from "../../support/agency/constants/bookingInformation";
import { common } from "../../support/webapp/modules/common";
import { agentHotel } from "../../support/agency/modules/hotelAgent";
import { serviceType } from "../../support/agency/constants/todo";

const username = Cypress.env('userForHotelCrossOriginMultipleOptions');
const agentUser = Cypress.env('agentUserForHotelBookingMultipleOptions');
const password = Cypress.env('adminPassword');
const url = Cypress.env('webappUrl');

describe('Send hotel options -> Confirm in webapp', () => {

    beforeEach(() => {
        cy.session('login webapp', () => {
            user.signIn(username, password);
        });
    });

    it('Delete the to dos and cancel the tasks - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        task.cancelTasks();
        toDo.deleteTodos();
    });

    it('Send hotel options - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.createToDo(serviceType.hotel);
        toDo.openToDo();
        agentHotel.sendHotelOptions(hotelName.mercureBucharest);
    });

    it('Confirm received option - Webapp', () => {
        cy.visit(url);
        hotel.confirmOptionSendViaChat(approvalReason, projectNumber);
    });

    it('Book hotel - Agency', () => {
        agent.signIn(agentUser, password);
        agent.openUserSession(username);
        toDo.openToDo();
        agentHotel.addTaskDetails(referenceNumber);
    });

    it('Check that the reference number was received - Webapp', () => {
        cy.visit(url);
        common.checkChatMessage(sentBy.agent, referenceNumber);
    });
});
